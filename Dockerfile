FROM alpine

RUN set -x \
	&& apk add --no-cache \
		binutils \
		g++ \
		gtk+3.0 \
		gtksourceview \
		make \
		py3-gobject3 \
		py3-pytest \
		python3 \
	&& pip3 install \
		black \
		pytest-cov
